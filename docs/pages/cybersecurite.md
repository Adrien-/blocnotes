# Cybersecurite

## Outils

### Enumération des ports ouverts

- **Namp**
    
    [Lien tuto](https://www.memoinfo.fr/tutoriels-linux/tuto-nmap-scaner-les-ports-ouverts/)
    
    `nmap -options ip_du_serveur`

    `nmap -sS -sU -sV -T3 192.168.1.101`
    
    
- **Zenmap**
    
    [Installation](https://lifars.com/2020/01/zenmap-installation-guide-kali-linux-2019-4/)

    Interface graphique nmap
    

### Enumération des répertoires d'un serveur / scan d'annuaire

- **didb**
    
    Trouver les répertoires cachés d'un serveur Web avec DIRB :

    `dirb http://academy.htb`
    
- **Gobuster**
    
    `gobuster dir -u [http://10.10.10.199](http://10.10.10.199/) -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`
    
    `gobuster dir -u [http://178.62.0.100:30480/administrat](http://178.62.0.100:30480/administrat) -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,html`
    

### Proxy

- **BrupSuite**
    
    [Installation](https://www.youtube.com/watch?v=CBspC2i_49k)
    
    burpsuite avec proxi et navigateur integrer 
    
    permer d'intercepter les communication entre le navigateur et le serveur
    
    ça permet de modifier la requette http a la main
    Burp Suite est une application Java, développée par PortSwigger Ltd, qui peut être utilisée pour la sécurisation ou effectuer des tests de pénétration sur les applications web
    
    Paramètres PROXY navigateur 

    

    
- **zaproxy**

### Exploitation de vulnérabilités

- **Metasploit**
    
    [https://h5ckfun.info/tutoriel-metasploit-pour-debutants-maitrisez-en-5-minutes/](https://h5ckfun.info/tutoriel-metasploit-pour-debutants-maitrisez-en-5-minutes/)


### Gestionnaire de versions (git)
- GitLeaks
  identification des secrets
  
- git-filter-rep
  reecriture de l'historique git (exmeple pour supprimer les decrets)

## Veille technologique

### Site de news

- [https://portswigger.net/daily-swig/](https://portswigger.net/daily-swig/)
- [https://thehackernews.com/](https://thehackernews.com/)


### Podcasts

- [https://www.nolimitsecu.fr/](https://www.nolimitsecu.fr/)
- [https://www.comptoirsecu.fr/podcast/](https://www.comptoirsecu.fr/podcast/)






