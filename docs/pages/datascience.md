# Data science

## Data processing

### Pandas

### Numpy

## Data visualisation

### Plotly

### Chart.js

### D3.js

### Folium

[Documentation](https://python-visualization.github.io/folium/index.html)

Folium permet de visualiser facilement les données manipulées en Python sur une carte interactive.

Folium s'appuie sur les forces de traitement des données de l'écosystème Python et sur les forces de cartographie de la bibliothèque leaflet.js.

### + de liens

[Source de dataviz](https://python-graph-gallery.com/)

