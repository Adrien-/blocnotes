# Gitlab

## CICD

### MkDocs

```
workflow:
  rules:
    - if: '$CI_COMMIT_MESSAGE =~ /deployFTP/'
      when: always
    - when: never

image: python:3.8

stages:
  - build
  - deploy

build-mkdocs:
  stage: build
  before_script:
    - pip install mkdocs==1.2.2
    - pip install mkdocs-material==7.2.1
    # Add your plugins
    - pip install pygments
    - pip install pymdown-extensions
    - pip install mkdocs-mermaid2-plugin
    - pip install mkdocs-awesome-pages-plugin
    - pip install pyembed-markdown
    - pip install mkdocs-git-revision-date-localized-plugin
    - pip install mkdocs-minify-plugin
  script:
    - mkdocs build
  artifacts:
    paths:
      - site/
    expire_in: 1 days
    untracked: false

EnvoiFTP:
  stage: deploy
  before_script:
  - apt-get update -qy
  - apt-get install -y lftp
  script:
    - lftp -e "set ssl:verify-certificate no; open $FTP_host_port; user $FTP_USERNAME $FTP_PASSWORD; mirror -X .* -X .*/ --reverse --verbose --delete site/ ./blocnote/ ; bye"
```

